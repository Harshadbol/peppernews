package com.harshad.newsbuddy.repository

import com.harshad.newsbuddy.data.NewsService
import com.harshad.newsbuddy.data.RetrofitInstance
import com.harshad.newsbuddy.data.model.NewsResponse


class NewsRepository {

    private val newsService = RetrofitInstance.getInstance().create(NewsService::class.java)

    suspend fun getNewsOnTopic(topicName: String): NewsResponse? {
        return try {
            val newRes = newsService.searchNews(topicName)
            if (newRes.status == "OK")
                newRes
            else
                null
        } catch (ex: Exception) {
            null
        }
    }

    fun getNewsTopicList(): List<String> {
        val newsTopics = mutableListOf<String>()
        newsTopics.add("Sports")
        newsTopics.add("Sports Adventure")
        newsTopics.add("Arts")
        newsTopics.add("Automobiles")
        newsTopics.add("Car")
        newsTopics.add("Books")
        newsTopics.add("Booming")
        newsTopics.add("Business")
        newsTopics.add("Education")
        newsTopics.add("Environment")
        newsTopics.add("Fashion And Style")
        newsTopics.add("Fashion")
        newsTopics.add("Style")
        newsTopics.add("Financial")
        newsTopics.add("Food")
        newsTopics.add("Health And Fitness")
        newsTopics.add("Health")
        newsTopics.add("Fitness")
        newsTopics.add("Technology")
        newsTopics.add("Science")
        newsTopics.add("Personal Tech")
        newsTopics.add("Movies")
        newsTopics.add("Travel")
        newsTopics.add("World")
        newsTopics.add("Society")
        newsTopics.add("Regionals")
        return newsTopics
    }
}