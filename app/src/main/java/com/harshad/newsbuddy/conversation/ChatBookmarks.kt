package com.harshad.newsbuddy.conversation

sealed class ChatBookmarks(val name: String) {
    object Welcome : ChatBookmarks("WELCOME")
    object WelcomeFinish:ChatBookmarks("WELCOME_FINISH")
    object SelectTopic : ChatBookmarks("NEWS_TOPICS")
    object TopicMessage : ChatBookmarks("TOPIC_MESSAGE")
    object Sports : ChatBookmarks("SPORT_NEWS")
    object Arts : ChatBookmarks("ARTS_NEWS")
    object Automobiles : ChatBookmarks("AUTOMOBILE_NEWS")
    object Technology : ChatBookmarks("TECHNOLOGY_NEWS")
    object Books:ChatBookmarks("BOOKS_NEWS")
}
