package com.harshad.newsbuddy.conversation

import android.content.Context
import android.util.Log
import androidx.lifecycle.ViewModel
import com.aldebaran.qi.Future
import com.aldebaran.qi.sdk.QiContext
import com.aldebaran.qi.sdk.builder.ChatBuilder
import com.aldebaran.qi.sdk.builder.QiChatbotBuilder
import com.aldebaran.qi.sdk.builder.TopicBuilder
import com.aldebaran.qi.sdk.`object`.conversation.AutonomousReactionImportance
import com.aldebaran.qi.sdk.`object`.conversation.AutonomousReactionValidity
import com.aldebaran.qi.sdk.`object`.conversation.Chat
import com.aldebaran.qi.sdk.`object`.conversation.QiChatExecutor
import com.aldebaran.qi.sdk.`object`.conversation.QiChatbot
import com.aldebaran.qi.sdk.`object`.conversation.Topic
import com.harshad.newsbuddy.listeners.UiEvents
import com.harshad.newsbuddy.viewmodel.NewsViewModel
import kotlinx.coroutines.DelicateCoroutinesApi
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.lang.ref.WeakReference

class PepperChatHandler(private val qiContext: QiContext) {

    private val TAG = "PepperChatHandler"
    lateinit var chatbot: QiChatbot
    lateinit var chat: Chat
    private lateinit var topic: Topic
    private var chatFuture: Future<Void>? = null
    private var uiEventListener = WeakReference<UiEvents>(null)
    private var mNewsViewModel: NewsViewModel? = null

    fun setBookMarkReachedListenerAndVM(listener: UiEvents, newsViewModel: NewsViewModel) {
        this.uiEventListener = WeakReference(listener)
        mNewsViewModel = newsViewModel
    }

    private fun initTopic() {
        topic = TopicBuilder.with(qiContext)
            .withAsset("News.top")
            .build()
    }

    private fun initChatBot() {
        if (this::chatbot.isInitialized) {
            chatbot.removeAllOnBookmarkReachedListeners()
        }
        chatbot = QiChatbotBuilder.with(qiContext)
            .withTopic(topic)
            .build()
    }

    private fun initChat() {
        chat = ChatBuilder.with(qiContext)
            .withChatbot(chatbot)
            .build()
    }

    fun stopChat() = chatFuture?.requestCancellation()

    @OptIn(DelicateCoroutinesApi::class)
    fun rebuildChatAndStartListening(onChatStarted: () -> Unit) {
        GlobalScope.launch {
            try {
                stopChat()
                initTopic()
                initChatBot()
                initExecutors()
                initChat()
                chatbot.addOnBookmarkReachedListener(
                    BookmarkReachedHandler(
                        uiEventListener,
                        this@PepperChatHandler
                    )
                )
                chatFuture = chat.async().run()
                chat.addOnStartedListener { onChatStarted() }
            } catch (exc: Exception) {
                Log.d(TAG, "Exception ${exc.localizedMessage}")
            }
        }
    }

    @OptIn(DelicateCoroutinesApi::class)
    fun gotoBookMark(bookmarks: ChatBookmarks) {
        GlobalScope.launch {
            val chatBookmark = topic.bookmarks[bookmarks.name]
            chatbot.async().goToBookmark(
                chatBookmark,
                AutonomousReactionImportance.HIGH,
                AutonomousReactionValidity.IMMEDIATE
            )
        }
    }

    private fun initExecutors() {
        val executors = hashMapOf<String, QiChatExecutor>()
        executors["newsExecutor"] = NewsExecutor(qiContext, this@PepperChatHandler, uiEventListener)
        chatbot.executors = executors
    }

}