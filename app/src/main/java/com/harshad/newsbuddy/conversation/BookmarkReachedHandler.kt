package com.harshad.newsbuddy.conversation

import android.util.Log
import com.aldebaran.qi.sdk.`object`.conversation.Bookmark
import com.aldebaran.qi.sdk.`object`.conversation.QiChatbot
import com.harshad.newsbuddy.listeners.UiEvents
import java.lang.ref.WeakReference

class BookmarkReachedHandler(
    private val uiEventListener: WeakReference<UiEvents>,
    private val pepperChatHandler: PepperChatHandler
) : QiChatbot.OnBookmarkReachedListener {
    private val TAG = "BookmarkReachedHandler"

    override fun onBookmarkReached(bookmark: Bookmark?) {
        Log.d(TAG, "onBookmarkReached: in ${bookmark?.name}")

        when (bookmark?.name) {
            ChatBookmarks.WelcomeFinish.name -> {
                //uiEventListener.get()?.onWelComeMsgFinished(true)
            }

            ChatBookmarks.SelectTopic.name -> {
                val newsTopic = pepperChatHandler.chatbot.variable("topicName").value
                uiEventListener.get()?.validateNewsTopic(newsTopic)
            }

            ChatBookmarks.Technology.name -> {
                val newsTopic = pepperChatHandler.chatbot.variable("topicName").value
                uiEventListener.get()?.getNewsByTopic(newsTopic)
            }

            ChatBookmarks.Books.name -> {
                val newsTopic = pepperChatHandler.chatbot.variable("topicName").value
                uiEventListener.get()?.getNewsByTopic(newsTopic)
            }

            ChatBookmarks.Arts.name -> {
                val newsTopic = pepperChatHandler.chatbot.variable("topicName").value
                uiEventListener.get()?.getNewsByTopic(newsTopic)
            }

            ChatBookmarks.Automobiles.name -> {
                val newsTopic = pepperChatHandler.chatbot.variable("topicName").value
                uiEventListener.get()?.getNewsByTopic(newsTopic)
            }

            ChatBookmarks.Sports.name -> {
                val newsTopic = pepperChatHandler.chatbot.variable("topicName").value
                uiEventListener.get()?.getNewsByTopic(newsTopic)
            }
        }
    }
}