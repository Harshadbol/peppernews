package com.harshad.newsbuddy.conversation



import com.aldebaran.qi.sdk.QiContext
import com.aldebaran.qi.sdk.`object`.conversation.BaseQiChatExecutor
import com.harshad.newsbuddy.listeners.UiEvents
import java.lang.ref.WeakReference


class NewsExecutor(
    qiContext: QiContext,
    val pepperChatHandler: PepperChatHandler,
    private val uiEventListener: WeakReference<UiEvents>,
) : BaseQiChatExecutor(qiContext) {

    private val TAG = "NewsExecutor"

    override fun runWith(params: MutableList<String>?) {
        val newsTopic = pepperChatHandler.chatbot.variable("topicName").value
        uiEventListener.get()?.getNewsByTopic(newsTopic)
    }

    override fun stop() {}
}