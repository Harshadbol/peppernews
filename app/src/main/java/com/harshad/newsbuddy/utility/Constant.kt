package com.harshad.newsbuddy.utility

object Constant {
    const val API_KEY = "VBjUbyyp7IL2ONIm4Zzxt2VFqAjEcGTE"
    const val BASE_URL = "https://api.nytimes.com"
    const val IMG_BASE_URL = "https://static01.nyt.com/"

    //news topics
    const val AdventureSports = "Adventure Sports"
    const val SportsAdventure = "Sports Adventure"
    const val Sports = "Sports"
    const val Arts = "Arts"
    const val Automobiles = "Automobiles"
    const val Car = "Car"
    const val Books = "Books"
    const val Technology = "Technology"
    const val Science = "Science"
    const val PersonalTech = "Personal Tech"
    const val Tech = "Tech"
    const val Booming = "Booming"
    const val Business = "Business"
    const val Education = "Education"
    const val Environment = "Environment"
    const val FashionAndStyle = "Fashion And Style"
    const val Fashion = "Fashion"
    const val Style = "Style"
    const val Financial = "Financial"
    const val Food = "Food"


}