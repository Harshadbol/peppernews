package com.harshad.newsbuddy

import android.os.Bundle
import android.widget.Toast
import androidx.activity.viewModels
import androidx.core.os.bundleOf
import androidx.lifecycle.MutableLiveData
import androidx.navigation.findNavController
import com.aldebaran.qi.sdk.QiContext
import com.aldebaran.qi.sdk.QiSDK
import com.aldebaran.qi.sdk.RobotLifecycleCallbacks
import com.aldebaran.qi.sdk.design.activity.RobotActivity
import com.harshad.newsbuddy.conversation.ChatBookmarks
import com.harshad.newsbuddy.conversation.PepperChatHandler
import com.harshad.newsbuddy.data.model.Docs
import com.harshad.newsbuddy.databinding.ActivityMainBinding
import com.harshad.newsbuddy.listeners.UiEvents
import com.harshad.newsbuddy.utility.Constant.AdventureSports
import com.harshad.newsbuddy.utility.Constant.Arts
import com.harshad.newsbuddy.utility.Constant.Automobiles
import com.harshad.newsbuddy.utility.Constant.Books
import com.harshad.newsbuddy.utility.Constant.Car
import com.harshad.newsbuddy.utility.Constant.PersonalTech
import com.harshad.newsbuddy.utility.Constant.Science
import com.harshad.newsbuddy.utility.Constant.Sports
import com.harshad.newsbuddy.utility.Constant.SportsAdventure
import com.harshad.newsbuddy.utility.Constant.Tech
import com.harshad.newsbuddy.utility.Constant.Technology
import com.harshad.newsbuddy.viewmodel.NewsViewModel
import kotlinx.coroutines.DelicateCoroutinesApi
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class MainActivity : RobotActivity(), RobotLifecycleCallbacks, UiEvents {

    private lateinit var binding: ActivityMainBinding
    private val mNewsViewModel by viewModels<NewsViewModel>()
    var chatHandler: PepperChatHandler? = null
    var mQiContext: QiContext? = null
    var onTopicSelectedLiveData = MutableLiveData<String>()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        QiSDK.register(this, this)
    }

    override fun onRobotFocusGained(qiContext: QiContext?) {
        mQiContext = qiContext
        mQiContext?.let { qiCtx ->
            if (chatHandler == null)
                chatHandler = PepperChatHandler(qiCtx)
        }
        chatHandler?.setBookMarkReachedListenerAndVM(this, mNewsViewModel)
        chatHandler?.rebuildChatAndStartListening(onChatStarted = {})
        //observeSelectedTopic()
    }

    override fun onRobotFocusLost() {
        //
    }

    override fun onRobotFocusRefused(reason: String?) {
        //
    }

    override fun onDestroy() {
        super.onDestroy()
        QiSDK.unregister(this, this)
    }

    private fun observeSelectedTopic() {
        onTopicSelectedLiveData.observe(this) { topicName ->
            chatHandler?.chatbot?.variable("topicName")?.value = topicName.capitalize()
            chatHandler?.gotoBookMark(ChatBookmarks.TopicMessage)
            val newsTopic = bundleOf()
            newsTopic.putString("newsTopic", topicName)
            findNavController(R.id.fl_container).navigate(R.id.newsFragment, newsTopic)
        }
    }

    fun getTopicFetchNews(topicName: String) {
        chatHandler?.chatbot?.variable("topicName")?.value = topicName.capitalize()
        chatHandler?.gotoBookMark(ChatBookmarks.TopicMessage)
        val newsTopic = bundleOf()
        newsTopic.putString("newsTopic", topicName)
        findNavController(R.id.fl_container).navigate(R.id.newsFragment, newsTopic)
    }

    override fun onWelComeMsgFinished(isWelcomeFinished: Boolean) {
        if (isWelcomeFinished) {
            chatHandler?.gotoBookMark(ChatBookmarks.SelectTopic)
        }
    }

    override fun getNewsByTopic(selectedNewsTopic: String) {
        navigateToNewsScreen(selectedNewsTopic)
    }

    override fun validateNewsTopic(newsTopicName: String) {
        when (newsTopicName) {
            AdventureSports, SportsAdventure, Sports -> {
                chatHandler?.gotoBookMark(ChatBookmarks.Sports)
            }

            Arts -> {
                chatHandler?.gotoBookMark(ChatBookmarks.Arts)
            }

            Automobiles, Car -> {
                chatHandler?.gotoBookMark(ChatBookmarks.Automobiles)
            }

            Books -> {
                chatHandler?.gotoBookMark(ChatBookmarks.Books)
            }

            Technology, Tech, PersonalTech, Science -> {
                chatHandler?.gotoBookMark(ChatBookmarks.Technology)
            }

        }
    }

    private fun navigateToNewsScreen(newsTopic: String) {
        val newsTopicMap = bundleOf()
        newsTopicMap.putString("newsTopic", newsTopic)
        findNavController(R.id.fl_container).navigate(R.id.newsFragment, newsTopicMap)
    }

    fun navigateToNewDetailsScreen(newsArticle: Docs?) {
        Toast.makeText(
            this,
            "We are working on news details Screen ${newsArticle?.abstract}",
            Toast.LENGTH_SHORT
        ).show()
    }
}