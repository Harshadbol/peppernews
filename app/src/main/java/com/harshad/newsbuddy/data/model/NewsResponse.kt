package com.harshad.newsbuddy.data.model

import com.google.gson.annotations.SerializedName


data class NewsResponse (

    @SerializedName("status"    ) var status    : String?   = null,
    @SerializedName("copyright" ) var copyright : String?   = null,
    @SerializedName("response"  ) var response  : Response? = Response()

)