package com.harshad.newsbuddy.data

import com.harshad.newsbuddy.data.model.NewsResponse
import com.harshad.newsbuddy.utility.Constant.API_KEY
import retrofit2.http.GET
import retrofit2.http.Query

interface NewsService {

    @GET("/svc/search/v2/articlesearch.json")
    suspend fun searchNews(
        @Query("q") topic: String,
        @Query("api-key") apikey: String = API_KEY
    ): NewsResponse

}