package com.harshad.newsbuddy.data.model


import com.google.gson.annotations.SerializedName


data class Response (

    @SerializedName("docs" ) var docs : ArrayList<Docs> = arrayListOf(),
    @SerializedName("meta" ) var meta : Meta?           = Meta()

)