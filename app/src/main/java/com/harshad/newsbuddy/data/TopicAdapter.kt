package com.harshad.newsbuddy.data

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.harshad.newsbuddy.databinding.LayoutNewsItemBinding

class TopicAdapter(val topics: List<String>, val onTopicClick: OnTopicClick) :
    RecyclerView.Adapter<TopicAdapter.TopicViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TopicViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = LayoutNewsItemBinding.inflate(inflater, parent, false)
        return TopicViewHolder(binding, onTopicClick)
    }

    override fun onBindViewHolder(holder: TopicViewHolder, position: Int) {
        holder.setData(topics[position])
    }

    override fun getItemCount(): Int {
        return topics.size
    }

    inner class TopicViewHolder(
        val binding: LayoutNewsItemBinding,
        val onTopicClick: OnTopicClick
    ) :
        RecyclerView.ViewHolder(binding.root) {

        fun setData(topicName: String) {
            binding.tvTopicName.text = topicName
            binding.clyTopic.setOnClickListener {
                onTopicClick.onItemSelected(topicName)
            }
        }
    }

    interface OnTopicClick {
        fun onItemSelected(topicName: String)
    }
}