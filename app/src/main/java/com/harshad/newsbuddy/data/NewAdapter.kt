package com.harshad.newsbuddy.data

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.harshad.newsbuddy.R
import com.harshad.newsbuddy.data.model.Docs
import com.harshad.newsbuddy.databinding.LayoutNewArticleItemBinding
import com.harshad.newsbuddy.utility.Constant.IMG_BASE_URL


class NewsAdapter(
    var newsList: List<Docs>?,
    val onNewsCardClick: OnNewsCardClick,
    val Ctx: Context
) :
    RecyclerView.Adapter<NewsAdapter.NewsViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NewsViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = LayoutNewArticleItemBinding.inflate(inflater, parent, false)
        return NewsViewHolder(binding, onNewsCardClick)
    }

    override fun onBindViewHolder(holder: NewsViewHolder, position: Int) {
        holder.setData(newsList?.get(position))
    }

    override fun getItemCount(): Int {
        return newsList?.size ?: 0
    }

    fun updateNews(newsList: List<Docs>?) {
        this.newsList = newsList
        notifyDataSetChanged()
    }

    inner class NewsViewHolder(
        val binding: LayoutNewArticleItemBinding,
        val onNewsCardClick: OnNewsCardClick
    ) :
        RecyclerView.ViewHolder(binding.root) {

        fun setData(newsArticle: Docs?) {
            binding.apply {
                tvDate.text = newsArticle?.pubDate ?: ""
                tvNewsHeadline.text = newsArticle?.abstract ?: ""
                tvSectionName.text = newsArticle?.sectionName ?: ""
                tvNewsSubHeadline.text = newsArticle?.leadParagraph ?: ""
                if (newsArticle?.multimedia != null && newsArticle.multimedia.size > 0) {
                    Glide.with(Ctx).load("$IMG_BASE_URL${newsArticle.multimedia[0].url}")
                        .placeholder(R.drawable.placeholder).into(imgNewsBanner)
                }
                //
                clyNewsArticle.setOnClickListener {
                    onNewsCardClick.onNewsItemClick(newsArticle)
                }
            }
        }
    }

    interface OnNewsCardClick {
        fun onNewsItemClick(newsArticle: Docs?)
    }

}