package com.harshad.newsbuddy.viewmodel

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.harshad.newsbuddy.data.model.Docs
import com.harshad.newsbuddy.repository.NewsRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch


class NewsViewModel : ViewModel() {

    var isApiCallInProgress = MutableLiveData(false)
    val newsArticlesLiveData = MutableLiveData<ArrayList<Docs>?>()
    private var currentApiCall: Job? = null
    val newsRepository = NewsRepository()

    fun getNewsOnParticularTopic(topicName: String) {
        if (isApiCallInProgress.value == true) {
            return
        }

        isApiCallInProgress.postValue(true)

        currentApiCall = viewModelScope.launch(Dispatchers.IO) {
            try {
                //here we have to call api
                val newResponse = newsRepository.getNewsOnTopic(topicName)
                if (newResponse != null) {
                    newsArticlesLiveData.postValue(newResponse.response?.docs)
                    isApiCallInProgress.postValue(false)
                } else {
                    newsArticlesLiveData.postValue(null)
                    isApiCallInProgress.postValue(false)
                }
            } catch (ex: Exception) {
                Log.d("ApiCall", "exception while news fetching ${ex.localizedMessage}")
            } finally {
                isApiCallInProgress.postValue(false)
            }
        }
    }

    fun getTopicList(): List<String> {
        return newsRepository.getNewsTopicList()
    }

    fun cancelApiCall() {
        if (currentApiCall != null) {
            currentApiCall?.cancel()
            isApiCallInProgress.postValue(false)
        }
    }


}