package com.harshad.newsbuddy.ui

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.viewModels
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModel
import androidx.recyclerview.widget.GridLayoutManager
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.dialog.MaterialDialogs
import com.harshad.newsbuddy.MainActivity
import com.harshad.newsbuddy.R
import com.harshad.newsbuddy.data.TopicAdapter
import com.harshad.newsbuddy.databinding.FragmentHomeBinding
import com.harshad.newsbuddy.viewmodel.NewsViewModel


class HomeFragment : Fragment(), TopicAdapter.OnTopicClick {

    private lateinit var binding: FragmentHomeBinding
    private val mNewsViewModel by viewModels<NewsViewModel>()
    lateinit var mainActivity: MainActivity

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentHomeBinding.inflate(inflater, container, false)
        initRecyclerView()
        return binding.root
    }

    private fun initRecyclerView() {
        val topics = mNewsViewModel.getTopicList()
        val topicAdapter = TopicAdapter(topics, this)
        binding.rvNewsTopics.layoutManager = GridLayoutManager(context, 4)
        binding.rvNewsTopics.adapter = topicAdapter
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mainActivity = activity as MainActivity
    }

    override fun onItemSelected(topicName: String) {
        mainActivity = activity as MainActivity
        mainActivity.getTopicFetchNews(topicName)
    }

}