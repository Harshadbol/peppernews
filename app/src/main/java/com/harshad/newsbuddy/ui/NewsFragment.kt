package com.harshad.newsbuddy.ui

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.harshad.newsbuddy.MainActivity
import com.harshad.newsbuddy.R
import com.harshad.newsbuddy.data.NewsAdapter
import com.harshad.newsbuddy.data.model.Docs
import com.harshad.newsbuddy.databinding.FragmentNewsBinding
import com.harshad.newsbuddy.viewmodel.NewsViewModel


class NewsFragment : Fragment(), NewsAdapter.OnNewsCardClick {

    private lateinit var binding: FragmentNewsBinding
    private lateinit var progressDialog: AlertDialog
    private val mNewsViewModel by viewModels<NewsViewModel>()
    private val args by navArgs<NewsFragmentArgs>()
    private lateinit var newsAdapter: NewsAdapter
    private var newsArticles = mutableListOf<Docs>()
    private lateinit var mainActivity: MainActivity

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View? {
        binding = FragmentNewsBinding.inflate(inflater, container, false)
        initProgressDialogAndRecyclerview()
        binding.tvTopicName.text = args.newsTopic ?: "Sports"
        fetchingNewsFromServer(args.newsTopic ?: "Sports")
        observeNews()
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mainActivity = activity as MainActivity
    }

    private fun fetchingNewsFromServer(newsTopic: String) {
        mNewsViewModel.getNewsOnParticularTopic(newsTopic)
    }

    private fun observeNews() {
        mNewsViewModel.isApiCallInProgress.observe(viewLifecycleOwner) { isNewsFetched ->
            if (isNewsFetched) {
                if (!progressDialog.isShowing) progressDialog.show()
            } else {
                if (progressDialog.isShowing) progressDialog.dismiss()
            }
        }

        mNewsViewModel.newsArticlesLiveData.observe(viewLifecycleOwner) { newsArticles ->
            if (newsArticles != null) {
                newsAdapter.updateNews(newsArticles)
            }
        }
    }

    private fun initProgressDialogAndRecyclerview() {
        progressDialog =
            MaterialAlertDialogBuilder(requireContext()).setView(R.layout.layout_progress_bar_dialog)
                .setCancelable(false).create()
        val layoutParams = WindowManager.LayoutParams()
        layoutParams.copyFrom(progressDialog.window?.attributes)
        layoutParams.width = WindowManager.LayoutParams.WRAP_CONTENT
        layoutParams.height = WindowManager.LayoutParams.WRAP_CONTENT
        progressDialog.window?.attributes = layoutParams
        //
        newsAdapter = NewsAdapter(newsArticles, this, requireContext())
        binding.rvNewsArticles.layoutManager = LinearLayoutManager(context)
        binding.rvNewsArticles.adapter = newsAdapter
    }

    override fun onNewsItemClick(newsArticle: Docs?) {
        mainActivity.navigateToNewDetailsScreen(newsArticle)
    }
}