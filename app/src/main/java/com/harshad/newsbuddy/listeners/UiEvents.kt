package com.harshad.newsbuddy.listeners

interface UiEvents {
    fun onWelComeMsgFinished(isWelcomeFinished:Boolean)
    fun getNewsByTopic(selectedNewsTopic: String)
    fun validateNewsTopic(newsTopicName:String)

}